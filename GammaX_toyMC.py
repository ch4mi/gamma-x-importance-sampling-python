import numpy as np
import matplotlib.pyplot as plt

def returnTracks(numEvents, E, lengthImportanceRegion = 5, IS_factor = 2, sampling = False, FVHeightAboveCathode = 2, lengthRFR = 14, lengthDrift = 140):
    """Simple implementation of importance sampling, using one important region.
    Returns the position of initial and scatter positions, and the corresponding deposited energies."""

    # Use this function to specify initial positions and angles - e.g. to simulate random events only in certain regions.
    y0, phi0 = getInitialPoints(numEvents, lengthRFR, lengthDrift, FVHeightAboveCathode = 0) # [cm]
    
    mfp0 = (0.095*E - 7.748)/10 # Linear approximation for mean free path [cm]
    d0 = np.random.exponential(scale = mfp0, size = len(y0)) # Sample from exponential PDF to get distance travelled [cm]
    
    y1 = y0 + d0 * np.cos(phi0) # Calculate new position [cm]
    
    if sampling:
        # Importance sampling step 
        y1Sampled, phi0Sampled, newNumEvents = importanceSample(y0, y1, phi0, lengthImportanceRegion, IS_factor)
    else: 
        y1Sampled = y1
        phi0Sampled = phi0
        newNumEvents = len(y0) 

    # Calculate energy deposited [keV] and scattered angle [rad]
    Edep0, deltaPhi = comptonAlgorithm(np.full(shape = newNumEvents, fill_value = E)) # Calculate energy deposit and angle scattered
    
    # Calculate new angle
    phi1 = phi0Sampled + np.random.choice([-1, 1], size = newNumEvents) * deltaPhi # Add or subtract Compton angle - since there is a symmetry in this projected 2D space
    E1 = E - Edep0 # Remaining energy of photon
    
    mfp1 = (0.095*E1 - 7.748)/10 # Mean free path of remaining photons
    d1 = np.random.exponential(scale = mfp1) # New distances travelled
    
    # Calculate new (2nd) scatter position and energy deposited
    y2 = y1Sampled + d1 * np.cos(phi1)
    Edep1, _ = comptonAlgorithm(E1)
    
    return y0, y1Sampled, y2, Edep0, Edep1

def returnTracksIS(numEvents, E, IScores, yB, yT, yDetectorBottom, yDetectorTop, FVHeightAboveCathode = 2, lengthRFR = 14, lengthDrift = 140):
    """General case for importance sampling with custom geometric cell divisions and weight assignments. 
    IScores is a list of importance scores corresponding to the region of import, from yB to yT. 
    If this region is properly contained within the detector, the remaining regions are assigned a score of unity.
    Returns the same quantities as the function above."""

    # Use this function to specify initial positions and angles - e.g. to simulate random events only in certain regions.
    y0, phi0 = getInitialPoints(numEvents, lengthRFR, lengthDrift, 0) # [cm]
    
    mfp0 = (0.095*E - 7.748)/10 # Linear approximation for mean free path [cm]
    d0 = np.random.exponential(scale = mfp0, size = len(y0)) # Sample from exponential PDF to get distance travelled [cm]
    
    y1 = y0 + d0 * np.cos(phi0) # Calculate new position
    
    # Variable names kept for consistency
    y0Sampled = y0
    y1Sampled = y1
    phi0Sampled = phi0

    # Get the possible transitions (list of tuples + corresponding score) and the edges of each geometric cell. 
    transitions, ISRegionEdges, _ = getTransitions(IScores, yB, yT, yDetectorBottom, yDetectorTop)

    # Loop through all the possible transitions and sample accordingly. Each transition should be disjoint so we loop directly.
    for (i, j), IS_factor in transitions:
        mask = getTransitionMask(y0Sampled, y1Sampled, ISRegionEdges, i, j) # Select events that satisfy each transition
        (y0Sampled, y1Sampled, phi0Sampled), newNumEvents = resample([y0Sampled, y1Sampled, phi0Sampled], mask, IS_factor) 

    # Calculate energy deposit [keV] and angle scattered [rad]
    Edep0, deltaPhi = comptonAlgorithm(np.full(shape = newNumEvents, fill_value = E)) 
    
    phi1 = phi0Sampled + np.random.choice([-1, 1], size = newNumEvents) * deltaPhi # Add or subtract Compton angle - since there is a symmetry in this projected 2D space
    E1 = E - Edep0 # Remaining energy of photon
    
    mfp1 = (0.095*E1 - 7.748)/10 # Mean free path of remaining photons
    d1 = np.random.exponential(scale = mfp1)
    
    y2 = y1Sampled + d1 * np.cos(phi1)
    Edep1, _ = comptonAlgorithm(E1)
    
    return y0, y1Sampled, y2, Edep0, Edep1
     
def getInitialPoints(numEvents, lengthRFR, lengthDrift, FVHeightAboveCathode):
    """Custom initial photon distribution (if required).
    Note that the cathode is at y = 0.
    Returns positions and angles."""
     
    totalLength = lengthDrift + lengthRFR
    newNumEvents = int(totalLength / (totalLength - FVHeightAboveCathode)) * numEvents
    
    y0 = np.random.uniform(low = -lengthRFR, high = lengthDrift, size = newNumEvents) # Starting y positions [cm]
    phi0 = np.random.uniform(low = 0, high = np.pi, size = newNumEvents) # Starting directions [rad]
    
    FVMask = np.logical_or(y0 < 0, y0 > FVHeightAboveCathode)
    
    return y0[FVMask], phi0[FVMask]
    
def comptonAlgorithm(E):
    """Fast algorithm based on https://www.sciencedirect.com/science/article/pii/0883288992902313
    Returns deposited energy [keV] and corresponding angle [rad]."""
    
    if isinstance(E, float) or isinstance(E, int):
        E = np.array([E])
    
    r = np.random.uniform(size = len(E))
    
    k = E / 511
    c0 = 2 * (2*k**2 + 2*k + 1) / (2*k + 1)**3
    b = (1 + c0/2) / (1 - c0/2)
    a = 2 * (b-1)
    
    x = b - (b+1)*np.power(c0/2, r)
    
    f = getF(k, x)
    h0 = a / (b-x)

    r2 = np.random.uniform(size = len(E))
    mask = f/h0 < r2 # if True, reiterate (failed)
    
    while mask.sum() > 0:
        r = np.random.uniform(size = mask.sum())
        r2 = np.random.uniform(size = mask.sum())
        x[mask] = b[mask] - (b[mask]+1)*np.power(c0[mask]/2, r) # Refill the failed entries with new randomly generated entry
        
        f = getF(k[mask], x[mask])
        h = h0[mask]
        
        mask[mask] = f/h < r2 # Update mask
        
    kPrime = 1 / (1 - x + 1/k)
    
    phi = np.arccos(x)
    Edep = (k - kPrime) * 511
    
    return Edep, phi

def getF(k, x):
    """Helper function for Compton algorithm."""

    fNum = 1 + x**2 + k**2 * (1-x)**2 / (1 + k * (1-x))
    fDenom = (1 + k * (1-x))**2
    f = fNum/fDenom
    return f

def importanceSample(yi, yf, phii, lengthImportanceRegion, IS_factor):
    """Get new tracks according to the simple importance sampling scheme. Also return the new number of events"""
    upsampleMask = np.logical_and(yi > lengthImportanceRegion, yf < lengthImportanceRegion)
    downsampleMask = np.logical_and(yi < lengthImportanceRegion, yf > lengthImportanceRegion)
    
    entriesToAdd = copyArrayNTimes(yf[upsampleMask], IS_factor - 2) # If twice as important, add a single copy
    phisToAdd = copyArrayNTimes(phii[upsampleMask], IS_factor - 2)
    
    choiceVector = [True] * (IS_factor - 1) + [False] # Downsample according to the IS - Russian roulette 
    randomMask = np.random.choice(choiceVector, size = downsampleMask.sum())
    entriesToRemove = yf[downsampleMask][randomMask]
    
#     print('Entries added: {}'.format(len(entriesToAdd)))
#     print('Entries removed: {}'.format(len(entriesToRemove)))

    yf = yf[~np.isin(yf, entriesToRemove)] # Remove elements
    yf = np.concatenate((yf, entriesToAdd)) # Add elements
    
    phii = phii[~np.isin(phii, phii[downsampleMask][randomMask])] # Remove elements
    phii = np.concatenate((phii, phisToAdd)) # Add elements
    
    return yf, phii, len(yf)

def getTransitions(importanceScores, yBottom, yTop, yDetectorBottom, yDetectorTop):
    """Return all possible transitions among geometric cells."""

    # Edges of the geometric cells 
    ISRegionEdges = [yBottom + i * (yTop - yBottom) / len(importanceScores) for i in range(len(importanceScores))] + [yTop]

    newScores = importanceScores

    if yTop < yDetectorTop: # Add a standard region to the top of the detector
        newScores = newScores + [1]
        ISRegionEdges.append(yDetectorTop) 

    if yDetectorBottom < yBottom: # Add a standard region to the bottom
        newScores = [1] + newScores
        ISRegionEdges.insert(0, yDetectorBottom)

    transitions = []
    for i, iScore in enumerate(newScores):
        for j, fScore in enumerate(newScores):
            r = fScore / iScore
            if r != 1: # No sampling required if cells have the same score
                transitions.append(((i, j), r))

    return transitions, ISRegionEdges, newScores

def getTransitionMask(yi, yf, ISRegionEdges, i, j):
    """Returns a mask that corresponds to the given transition (i, j)"""

    yi_in_i = np.logical_and(yi > ISRegionEdges[i], yi < ISRegionEdges[i+1])
    yf_in_j = np.logical_and(yf > ISRegionEdges[j], yf < ISRegionEdges[j+1]) 
            
    return np.logical_and(yi_in_i, yf_in_j)

def resample(arrays, mask, IS_factor):
    """Upsample or downsample the given arrays according to the importance factor. Return the sampled arrays and the new length of each."""

    result = []

    if IS_factor > 1: # Upsample
        for arr in arrays:
            elementsToAdd = copyArrayNTimes(arr[mask], IS_factor - 2)
            result.append(np.concatenate((arr, elementsToAdd)))

    else: # Downsample
        factor_ = int(1/IS_factor)
        choiceVector = [True] * (factor_ - 1) + [False] # Russian roulette
        randomMask = np.random.choice(choiceVector, size = mask.sum())
        for arr in arrays:
            result.append(arr[~np.isin(arr, arr[mask][randomMask])]) # Remove the deleted tracks

    return result, len(result[0])

def copyArrayNTimes(arr, n):
    """Helper function for sampling."""
    result = arr
    for i in range(int(n)):
        result = np.concatenate((result, arr))
    return result
    
def getGXRate(fiducialLength, y1, y2):
    """Get the gamma-X rate given a FV, and the positions of each scatter.""" 

    RFRMask = y1*y2 < 0 # One scatter is below cathode
    fiducialMask = np.logical_or(y1 > fiducialLength, y2 > fiducialLength) # Other scatter is above the fiducial cut
    
    finalMask = RFRMask * fiducialMask
    return finalMask.sum() / len(finalMask)

def plotScheme(scores, edges, FV = 1):
    """Plotting function for weight assignment.""" 

    edges = np.array(edges)
    scores = np.array(scores)
    widths = edges[1:] - edges[:-1]
    centers = (edges[1:] + edges[:-1]) / 2

    yMin = centers[scores != 1][0] - 2*widths.min()
    yMax = centers[scores != 1][-1] + 2*widths.min()

    plt.barh(centers, scores, widths, alpha = 0.4, edgecolor = 'gray')

    plt.hlines(0, xmin = 0, xmax = 2*scores.max(), color = 'gray', ls = '--', lw = 2, label = 'cathode')
    plt.hlines(FV, xmin = 0, xmax = 2*scores.max(), color = 'gray', ls = ':', lw = 2, label = 'FV')
    plt.xscale('log', basex = 2)
    plt.xlabel('Importance Score')
    plt.ylabel('Height [cm]')
    plt.ylim(yMin, yMax)
    plt.legend()
    return